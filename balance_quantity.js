const Account = require("./lib/account");
const data = require("./data.json");

const balanceQuantity = async () => {
  let account = new Account(data.config);
  let result = await account.login("lili.tran@LINA.network", "123abc!!");
  if (!result) throw "login failed";
  account.ID = 2;
  // prepare tracability input
  let input = {
    // status: 1,
    arableLandIds: [1],
  };

  return await account.queryData("getBalanceQuantityData", { customs: [{
        "@type": "proto.BalanceQuantityInput",
        ...input
      }], languageId: 1 });
};

const balanceQuantityDetail = async () => {
  let account = new Account(data.config);
  let result = await account.login("lili.hyperr@LINA.network", "123abc!!");
  if (!result) throw "login failed";
  account.ID = 4;
  // prepare tracability input
  let input = {
    status: 1,
    arableLandIds: [4],
    seasonId: 2
  };

  return await account.queryData("getBalanceQuantityDetail", { customs: [{
      "@type": "proto.BalanceQuantityInput",
      ...input
    }], languageId: 1 });
};

balanceQuantity().then((rs) => {
  console.log(JSON.stringify(rs))
});