syntax = "proto3";

package proto;

option go_package = "proto";
import "google/protobuf/timestamp.proto";
import "google/protobuf/any.proto";
import "types.proto";
import "blockchain.proto";
import "harvests.proto";

message Response {
    enum statusCode {
        default = 0;
        OK = 200;
        Internal = 500;
        BadRequest = 400;
    }
    statusCode status = 1;
    google.protobuf.Any message = 2;
}

message CommonInput {
    enum orderType {
        DESC = 0;
        ASC = 1;
    }
    message Order {
        orderType type = 1;
        string field = 2;
    }
    string fromField = 1;
    int32 from = 2;
    int32 limit = 3;
    int32 languageId = 4;
    Order order = 6;
    repeated google.protobuf.Any customs = 7;
    string select = 8;
}

enum pesticideType {
    pesticideTypeNone = 0;
    pesticideTypePesticide = 1;
    pesticideTypeStimulateGrowth = 2;
}

enum cultivationType {
    cultivationTypeNone = 0;
    cultivationTypeFertilizer = 1;
    cultivationTypePesticide = 2;
    cultivationTypeStimulateGrowth = 3;
    cultivationTypeWater = 4;
    cultivationTypeTest = 5;
    cultivationTypeCareHistory = 6;
}

enum materialType {
    materialTypeNode = 0;
    materialTypeSeedling = 1;
    materialTypeFertilizer = 2;
    materialTypePesticide = 3;
}

enum seasonStatus {
    seasonStatusNone = 0;
    seasonInSeedling = 1;
    seasonInHarvesting = 2;
    seasonFinished = 3;
    seasonDeleted = 4;
}

enum seedCultivateType {
    seedCultivateTypeNone = 0;
    seedCultivateTypeNew = 1;
    seedCultivateTypeOld = 2;
}


message Farms {
    message IDName {
        int32 id = 1;
        string name = 2;
    }
    repeated IDName data = 1;
}

message PasswordMessage {
    string email = 1;
    string password = 2;
}

message ChangePasswordMessage {
    string email = 1;
    string oldPassword = 2;
    string newPassword = 3;
}

message UserProfiles {
    repeated UserProfile Data = 1;
}

message UserInfo {
    string address = 1;
    string privateKey = 2;
}

message UserPermissions {
    int32 farmId = 1;
    string idHash = 2;
    string owner = 3;
    string address = 4;
    repeated string permissions = 5;
}

message RegisterInfo {
    string email = 1;
    string address = 2;
    string cipher = 3;
    string kdf = 4;
    string cipherText = 5;
    string iv = 6;
    string salt = 7;
    string mac = 8;
    int32 timestamp = 9;
    int32 version = 10;
    string privateKey = 11;
}

message TransactionSignatureHash {
    string signatureHash = 2;
}

message EmptyMessage {}

message BytesMessage {
    bytes message = 1;
}

message StringMessage {
    string message = 1;
}

message StringMessages {
    repeated string data = 1;
}

message Int32Message {
    int32 message = 1;
}

message MaterialReportParamsMessage {
    int32 materialType = 1;
    int32 materialId = 2;
    int32 materialLotId = 3;
    google.protobuf.Timestamp date = 4;
    int32 languageId = 5;
}

message FinishedProductReportParamsMessage {
    int32 finishedProductId = 1;
    int32 gradedProductId = 2;
    int32 lotProductId = 3;
    google.protobuf.Timestamp date = 4;
    int32 languageId = 5;
}

message Int32Messages {
    repeated int32 data = 1;
}

message BoolMessage {
    bool message = 1;
}

message Languages {
    repeated Language data = 1;
}

message GetWorkingFarmsInput {
    string address = 1;
    int32 languageId = 2;
}

message SeedlingInfo {
    int32 id = 1;
    string code = 2;
    string name = 3;
    int32 unitId = 4;
    string unit = 5;
    int32 originSeedlingId = 6;
    string originSeedling = 7;
    int32 finishedProductId = 8;
    string finishedProductCode = 9;
    string finishedProductName = 10;
    string gtin = 11;
    status status = 12;
    int32 languageId = 13;
    string createdBy = 14;
    string createdTx = 15;
    string modifiedBy = 16;
    string modifiedTx = 17;
}

message Seedlings {
    repeated SeedlingInfo data = 1;
}

message OriginSeedlingInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message OriginSeedlings {
    repeated OriginSeedlingInfo data = 1;
}

message ProviderInfo {
    int32 id = 1;
    string businessLicense = 2;
    string gln = 3;
    string legal = 4;
    int32 languageId = 5;
    string name = 6;
    status status = 7;
    string createdBy = 8;
    string createdTx = 9;
    string modifiedBy = 10;
    string modifiedTx = 11;
}

message Providers {
    repeated ProviderInfo data = 1;
}

message CategoryFertilizerInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message CategoryFertilizers {
    repeated CategoryFertilizerInfo data = 1;
}

message CategoryPesticideInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message CategoryPesticides {
    repeated CategoryPesticideInfo data = 1;
}

message StoreHouseInfo {
    int32 id = 1;
    int32 farmId = 2;
    int32 languageId = 3;
    string name = 4;
    string farm = 5;
    status status = 6;
    string createdBy = 7;
    string createdTx = 8;
    string modifiedBy = 9;
    string modifiedTx = 10;
}

message StoreHouses {
    repeated StoreHouseInfo data = 1;
}

message PurposeInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message Purposes {
    repeated PurposeInfo data = 1;
}

message GradedProducts {
    repeated GradedProduct data = 1;
}

message CertificateInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    string createdBy = 4;
    string createdTx = 5;
    string modifiedBy = 6;
    string modifiedTx = 7;
}

message Certificates {
    repeated CertificateInfo data = 1;
}

message SoilInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message Soils {
    repeated SoilInfo data = 1;
}

message WaterInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message Waters {
    repeated WaterInfo data = 1;
}

message QACenterInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message QACenters {
    repeated QACenterInfo data = 1;
}

message PesticideInfo {
    int32 id = 1;
    string code = 21;
    string name = 2;
    int32 languageId = 3;
    int32 categoryId = 4;
    string category = 5;
    int32 typeId = 6;
    string gtin = 7;
    int32 unitId = 8;
    string unit = 9;
    int32 unitDisplayId = 10;
    string unitDisplay = 11;
    double grossWeight = 12;
    double netWeight = 13;
    int32 unitVolumeId = 14;
    string unitVolume = 15;
    double volume = 16;
    int32 unitDimensionId = 17;
    string unitDimension = 18;
    double dimension = 19;
    status status = 20;
    string createdBy = 22;
    string createdTx = 23;
    string modifiedBy = 24;
    string modifiedTx = 25;
    int32 unitWeightId = 26;
    string unitWeight = 27;
}

message Pesticides {
    repeated PesticideInfo data = 1;
}

message FertilizerInfo {
    int32 id = 1;
    string code = 20;
    string name = 2;
    int32 languageId = 3;
    int32 categoryId = 4;
    string category = 5;
    string gtin = 6;
    int32 unitId = 7;
    string unit = 8;
    int32 unitDisplayId = 9;
    string unitDisplay = 10;
    double grossWeight = 11;
    double netWeight = 12;
    int32 unitVolumeId = 13;
    string unitVolume = 14;
    double volume = 15;
    int32 unitDimensionId = 16;
    string unitDimension = 17;
    double dimension = 18;
    status status = 19;
    string createdBy = 21;
    string createdTx = 22;
    string modifiedBy = 23;
    string modifiedTx = 24;
    int32 unitWeightId = 25;
    string unitWeight = 26;
}

message Fertilizers {
    repeated FertilizerInfo data = 1;
}

message DiseaseInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message Diseases {
    repeated DiseaseInfo data = 1;
}

message MethodFertilizerInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message MethodFertilizers {
    repeated MethodFertilizerInfo data = 1;
}

message MethodPesticideInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message MethodPesticides {
    repeated MethodPesticideInfo data = 1;
}

message MethodWaterInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message MethodWaters {
    repeated MethodWaterInfo data = 1;
}

message MethodWasteTreatmentInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message MethodWasteTreatments {
    repeated MethodWasteTreatmentInfo data = 1;
}

message MethodCareInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message MethodCares {
    repeated MethodCareInfo data = 1;
}

message WeatherInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message Weathers {
    repeated WeatherInfo data = 1;
}

message CareActivityInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message CareActivities {
    repeated CareActivityInfo data = 1;
}

message ContainerToolInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message ContainerTools {
    repeated ContainerToolInfo data = 1;
}

message PaymentMethodInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message PaymentMethods {
    repeated PaymentMethodInfo data = 1;
}

message TransportInfo {
    int32 id = 1;
    string name = 2;
    status status = 3;
    int32 languageId = 4;
    string createdBy = 5;
    string createdTx = 6;
    string modifiedBy = 7;
    string modifiedTx = 8;
}

message Transports {
    repeated TransportInfo data = 1;
}

message TracabilityInput {
    int32 finishedProductId = 1;
    int32 gradedProductId = 2;
    int32 lotProductId = 3;
    int32 finishedProductExportId = 4;
    int32 languageId = 5;
}

message FarmCertificateReport {
    string name = 1;
    google.protobuf.Timestamp publishedDate = 2;
    google.protobuf.Timestamp expiredDate = 3;
    string issuedBy = 4;
}

message TracabilityInfo {
    message FinishedProductInfo {
        string gradedCode = 1;
        string gradedName = 2;
        string finishedProductName = 3;
        string finishedProductCode = 4;
    }
    message LotProduct {
        int32 id = 1;
        string name = 2;
        string sectionLand = 3;
        string season = 4;
        string generalSeason = 5;
    }
    message FarmInfo {

        message TestResult {
            google.protobuf.Timestamp publishedDate = 1;
            string issuedBy = 2;
        }
        int32 farmId = 1;
        string name = 2;
        string address = 3;
        string email = 4;
        string phone = 5;
        string sectionLand = 6;
        repeated FarmCertificateReport certificates = 7;
        TestResult soil = 8;
        TestResult water = 9;
        TestResult finishedProduct = 10;
    }
    message LotProductInfo {
        string name = 1;
        google.protobuf.Timestamp executedDate = 2;
        string archives = 3;
        string containerTool = 4;
        double quantity = 5;
        string unit = 6;
        string executedBy = 7;
    }
    FinishedProductInfo finishedProductInfo = 1;
    OrderProductInfo orderInfo = 2;
    LotProductInfo lotProductInfo = 3;
    FarmInfo farmInfo = 4;
    repeated LotProduct lotProducts = 5;
}

message TracabilityCultivationTests {
    message CultivationTestResult {
        double age = 1;
        int32 quantity = 2;
    }
    message CultivationTest {
        string disease = 1;
        google.protobuf.Timestamp executedDate = 2;
        repeated CultivationTestResult testResults = 3;
    }
    repeated CultivationTest cultivationTests = 1;
}

message TracabilityCareHistories {
    message CareHistory {
        string careActivity = 1;
        google.protobuf.Timestamp careHistoryExecutedDate = 2;
        string methodWasteTreatment = 3;
        google.protobuf.Timestamp methodWasteTreatmentDate = 4;
    }
    repeated CareHistory careHistories = 1;
}

message TracabilityTimeLine {
    enum TimeLineType {
        TimeStart = 0;
        Seedling = 1;
        CultivationCare = 2;
        Harvest = 3;
        Order = 4;
        Export = 5;
    }
    message SeedlingCultivation {
        int32 lotSeedlingId = 1;
        string seedlingName = 2;
        int32 quantity = 3;
        double age = 4;
    }
    message HarvestInfo {
        string executedBy = 1;
        string archives = 2;
        int32 quarantine = 3;
    }
    message OrderInfo {
        string customer = 1;
        google.protobuf.Timestamp exportedDate = 2;
        double quantity = 3;
        string unit = 4;
        int32 storedTime = 5;
    }
    message ExportInfo {
        string customer = 1;
        google.protobuf.Timestamp exportedDate = 2;
    }
    message TimeLine {
        google.protobuf.Timestamp time = 1;
        TimeLineType type = 2;
        SeedlingCultivation seedlingCultivation = 3;
        HarvestInfo harvest = 4;
        OrderInfo order = 5;
        ExportInfo export = 6;
    }
    int32 seasonId = 1;
    repeated TimeLine timeLine = 2;
}

message ExportTicketCodes {
    message ExportTicketCode {
        int32 finishedProductExportId = 1;
        string purchaserCode = 2;
        string purchaserName = 3;
        string exportCode = 4;
    }
    repeated ExportTicketCode data = 1;
}

message BalanceQuantityInput {
    int32 status = 1;
    repeated int32 arableLandIds = 2;
    int32 seasonId = 3;
}

message BalanceQuantity {
    message balanceQuantityData {
        int32 seasonId = 1;
        string sectionLand = 2;
        string season = 3;
        string generalSeason = 4;
        string seedling = 5;
        string unit = 6;
        double expectedAvg = 7;
        double actual = 8;
        int32 status = 9;
        int32 arableLandId = 10;
    }
    repeated balanceQuantityData data = 1;
}

message BalanceQuantityDetail {
    string sectionLand = 1;
    string season = 2;
    string generalSeason = 3;
    string seedling = 4;

    message seedlingDetail {
        double age = 1;
        int32 quantity = 2;
    }

    message estimatedHarvestInfo {
        double avg = 1;
        double highest = 2;
        double lowest = 3;
    }

    repeated seedlingDetail seedlingInfo = 5;
    estimatedHarvestInfo expectedHarvest = 6;

    google.protobuf.Timestamp estimatedEndDate = 7;
    double actualQuantity = 8;
    google.protobuf.Timestamp endDate = 9;
}

message TraderReportInput {
    repeated int32 farmIds = 1;
    repeated int32 finishedProductIds = 2;
}


message TraderReportFilterOutput {
    message idName {
        int32 id = 1;
        string name = 2;
    }
    repeated idName data = 1;
}

message TraderReportOutput {
    message FinishedGradedProduct {
        int32 seasonId = 1;
        int32 finishedProductId = 2;
        string finishedProduct = 3;
        string gradedProduct = 4;
        double estimatedHarvest = 5;
        double actualHarvest = 6;
        double inventory = 7;
        google.protobuf.Timestamp estimatedEndDate = 8;
        string unit = 9;
        int32 seasonStatus = 10;
    }
    message FarmInfo {
        string name = 1;
        string address = 2;
        int32 area = 3;
        string areaUnit = 4;
        repeated FarmCertificateReport certificates = 5;
        repeated FinishedGradedProduct finishedGradedProducts = 6;
        string ownerName = 7;

    }
    repeated FarmInfo farms = 1;
}

message TraderAccessFarmInput {
    string userAddress = 1;
    repeated int32 enabledFarmIds = 2;
    repeated int32 disabledFarmIds = 3;
}

message TraderAccessFarmOutput {
    string userAddress = 1;
    message FarmInfo {
        int32 id = 1;
        string name = 2;
        int32 status = 3;
    }
    repeated FarmInfo farmInfo = 2;
}
