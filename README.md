# This repo is used to demo how to interact with LINA Supply chain framework

## Requirements:

- Blockchain and API are running

## Install:

```
npm install
```

## Run:

```
npm start
```
