const Account = require("./lib/account");
const data = require("./data.json");

const tracability = async () => {
  let account = new Account(data.config);
  let result = await account.login("lili.tran@LINA.network", "123abc!!");
  if (!result) throw "login failed";
  account.ID = 2;
  // prepare tracability input
  let input = {
    finishedProductId: 3,
    gradedProductId: 1,
    // finishedProductExportId: 3,
    lotProductId: 5
  };

  return {
    internal: {
      // info: await account.queryData("getTracabilityInfo", { customs: [{
      //     "@type": "proto.TracabilityInput",
      //     ...input
      //   }], languageId: 1 }),
      // timeline: await account.queryData("getTracabilityTimeline", { customs: [{
      //     "@type": "proto.TracabilityInput",
      //     // lotProductId: 5,
      //     ...input
      //   }], languageId: 1 }),
      // cultivationTests: await account.queryData("getTracabilityCultivationTests", { customs: [{
      //     "@type": "proto.Int32Message",
      //     message: 2
      // }] }),
      // careHistories: await account.queryData("getTracabilityCareHistories", { customs: [{
      //     "@type": "proto.Int32Message",
      //     message: 2
      //   }] })
    },
    external: {
      // info: await account.getTracabilityInfo(5, 1),
      // timeline: await account.getTracabilityTimeline(5, 1),
      // getLotProductId: await account.getLotProductIdByName("SRGTR_SRR6S_20200606_1"),
      getPesticideCultivations: await account.getCultivations("pesticide", 2, {
        languageId: 1,
        customs: [
          {
          "@type": "proto.Int32Messages",
          "data": [3], // seasonIds - this data is optional
          },
          {
            "@type": "proto.Int32Message",
            "message": 1,
          },
        ]
      }),
      getFertilizerCultivations: await account.getCultivations("fertilizer", 2, {
        languageId: 1,
        customs: [
          {
            "@type": "proto.Int32Messages",
            "data": [3], // seasonIds - this data is optional
          },
        ]
      }),
    }
  };
};

tracability().then((rs) => {
  console.log(JSON.stringify(rs))
}).catch((err) => {
  console.log(err)
});