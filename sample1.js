const Account = require("./lib/account");

const data = require("./data.json");
const RootPrivateKey =
  "0x8843ebcb1021b00ae9a644db6617f9c6d870e5fd53624cefe374c1d2d710fd06";
const RootAddress = "0xc1fe56E3F58D3244F606306611a5d10c8333f1f6";

const randomText = (length) => {
  let result = "";
  let characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const randomNumbers = (length) => {
  let result = "";
  let characters = "0123456789";
  let charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
};

const randomDateStart = () => {
  const start =
    new Date(
      2019,
      Math.floor(Math.random() * 12) + 1,
      Math.floor(Math.random() * 28) + 1
    ).getTime() / 1000;
  return Math.floor(start);
};

const randomDateEnd = () => {
  const start = new Date();
  let end = new Date(2020 + Math.floor(Math.random() * 5), 1, 1);
  end =
    new Date(
      start.getTime() + Math.random() * (end.getTime() - start.getTime())
    ).getTime() / 1000;
  return Math.floor(end);
};

const makeEmail = (length) => {
  return `${randomText(length)}@gmail.com`;
};

const setupMetadata = async () => {
  // root account
  // add meta data. such as: language
  let rootAccount = new Account({
    address: RootAddress,
    privateKey: RootPrivateKey,
    ...data.config,
  });

  let vietnamInfo = undefined;
  let vietnamId = undefined;
  try {
    vietnamInfo = await rootAccount.queryData("getLanguage", { languageId: 1 });
    vietnamId = { message: vietnamInfo.id };
  } catch (e) {
    console.error(e);

    console.log("create root unit");
    vietnamId = await rootAccount.messageFactory(
      await rootAccount.sendRequest(
        "addLanguage",
        true,
        data.requestType.language,
        { language: "Vietnamese" }
      )
    );
    console.log(`vietnamese has been added at id=${vietnamId.message}`);
  }

  let englishInfo = undefined;
  let englishId = undefined;
  try {
    englishInfo = await rootAccount.queryData("getLanguage", { languageId: 2 });
    englishId = { message: englishInfo.id };
  } catch (e) {
    console.error(e);

    console.log("create root unit");
    englishId = await rootAccount.messageFactory(
      await rootAccount.sendRequest(
        "addLanguage",
        true,
        data.requestType.language,
        { language: "English" }
      )
    );
    console.log(`vietnamese has been added at id=${englishId.message}`);
  }

  let unitInfo = undefined;
  let unitId = undefined;
  try {
    unitInfo = await rootAccount.queryData("getUnits", {
      fromField: "Id",
      languageId: vietnamId.message,
    });
    unitId = unitInfo.data[0].id;
  } catch (e) {
    console.error(e);

    console.log("create root unit");
    unitId = await rootAccount.messageFactory(
      await rootAccount.sendRequest(
        "addUnit",
        true,
        data.requestType.unitInfo,
        { name: "unit1", languageId: vietnamId.message }
      )
    );
    unitId = unitId.message;
    console.log(`vietnamese has been added at id=${vietnamId.message}`);
    console.log(`english has been added at id=${englishId.message}`);
  }
  console.log(`unit unit1 has been added at id=${unitId}`);

  return { vietnamese: vietnamId.message, unitId: unitId };
};

const main = async () => {
  let result = await setupMetadata();
  result = {
    vietnamese: result.vietnamese,
    email: "macnguyenkhoi4@gmail.com",
    password: "Alo123ma!!",
    unitId: result.unitId,
  };

  let account = new Account(data.config);
  account.LanguageID = result.vietnamese;

  // login.
  // note that farm must update its name at least 1 language in order to login.
  try {
    await account.login(result.email, result.password);
  } catch (e) {
    console.error(e);

    // register
    await doNewAcc(result, account);
    await account.login(result.email, result.password);
  }
  console.log(`address=${account.Address}  privateKey=${account.PrivateKey}}`);

  // get list of working farms
  let farms = await account.getWorkingFarms();
  console.log(`list working farm is ${JSON.stringify(farms)}`);

  const temp = await account.loadAllMethods();
  console.log(`********** ${JSON.stringify(temp)}`);

  await account.getPermissions(farms.data[0].id);
  console.log(`****${account.Permissions.length}`);
  console.log(
    `permissions of address ${account.Address} ID=${
      account.ID
    } is ${JSON.stringify(account.Permissions)}`
  );

  let checkList = await account.queryData("getHarvests", {
    fromField: "Id",
    languageId: result.vietnamese,
    order: { field: "CreatedAt", type: 1 },
    customs: [
      {
        "@type": "proto.Int32Messages",
        message: [1],
        data: [1],
      },
    ],
  });

  //await doSection(result, account);

  //await doFarmCertificate(result, account);
};

const doNewAcc = async (result, account) => {
  // register
  await account.register(result.email, result.password);
  console.log(
    `new created account is ${account.Address} && ${account.PrivateKey}`
  );

  let farmInfoData = {
    name: randomText(20),
    address: randomText(20),
    code: randomText(10),
    glnCode: randomText(10),
    area: randomNumbers(1),
    businessLicense: randomText(10),
    email: makeEmail(6),
    phone: randomNumbers(8),
    languageId: result.vietnamese,
  };
  // register new farm and add permission
  let rs = await account.registerFarm(farmInfoData);
  if (!rs) {
    throw "registerFarmFailed";
  }

  await account.getPermissions(account.ID);
  console.log(`****${account.Permissions.length}`);
  account.Permissions.forEach((value) => console.log(`**${value}`));
};

const doSection = async (result, account) => {
  try {
    let sectionList = undefined;
    let sectionIds = [];
    try {
      sectionList = await account.queryData("getSections", {
        fromField: "Id",
        from: 0,
        languageId: result.vietnamese,
        order: { field: "CreatedAt", type: 1 },
      });
      sectionList.data.forEach((section) => {
        sectionIds.push(section.id);
      });
      console.log(JSON.stringify(sectionIds));
    } catch (e) {
      console.error(e);
      console.log("add section");

      // add section
      // loop 100 times and add section with random generated fields.
      console.log("sending addSection requests");
      for (let i = 0; i < 20; i++) {
        let sectionInfo = {
          name: randomText(6),
          sectionCode: randomText(3),
          glnCode: randomText(10),
          location: randomText(10),
          unitId: result.unitId,
          area: Math.floor(Math.random() * 10),
          manageBy: account.Address,
          phone: randomNumbers(9),
          email: makeEmail(6),
          status: Math.floor(Math.random() * 2),
          languageId: result.vietnamese,
          farmId: account.ID,
        };
        let sectionId = await account.messageFactory(
          await account.sendRequest(
            "addSection",
            true,
            data.requestType.sectionInfo,
            sectionInfo
          )
        );
        sectionIds.push(sectionId.message);
      }
      console.log(JSON.stringify(sectionIds));
    }

    console.log("\n\n");
    // paging section with limit=10 per page
    let i = 0;
    let counter = 1;
    let from = sectionIds[0];
    while (i < 20) {
      console.log(
        "************************************************SECTION*************************************************************************"
      );
      console.log(`page ${counter}`);
      let sectionList = await account.queryData("getSections", {
        fromField: "Id",
        from: from,
        limit: 10,
        languageId: result.vietnamese,
        order: { field: "CreatedAt", type: 1 },
      }); //ASC
      if (sectionList.data.length === 0) {
        break;
      }

      for (let j = 0; j < sectionList.data.length; j++) {
        console.log(
          account.toObject(data.requestType.sectionInfo, sectionList.data[j])
        );
      }
      from = sectionList.data[sectionList.data.length - 1].id + 1;
      i += 10;
      counter++;
    }
  } catch (e) {
    console.log(e);
  }
};

const doFarmCertificate = async (result, account) => {
  try {
    let farmCertificateList = undefined;
    let farmCertificateIds = [];
    try {
      farmCertificateList = await account.queryData("getFarmCertificates", {
        fromField: "Id",
        languageId: result.vietnamese,
        order: { field: "CreatedAt", type: 1 },
      });
      farmCertificateList.data.forEach((farmCertificate) => {
        farmCertificateIds.push(farmCertificate.id);
      });
      console.log(JSON.stringify(farmCertificateIds));
    } catch (e) {
      console.error(e);
      console.log("add certificate");

      for (let i = 0; i < 20; i++) {
        let farmCertificateInfo = {
          code: randomText(20),
          certificateId: Math.floor(Math.random() * 2) + 1,
          certificateName: randomText(10),
          issuedDate: { seconds: randomDateStart() },
          expiredDate: { seconds: randomDateEnd() },
          issuedBy: randomText(20),
          attach: randomText(20),
          status: Math.floor(Math.random() * 2),
          farmId: account.ID,
        };
        let farmCertificateId = await account.messageFactory(
          await account.sendRequest(
            "addFarmCertificate",
            true,
            data.requestType.farmCertificateInfo,
            farmCertificateInfo
          )
        );
        farmCertificateIds.push(farmCertificateId.message);
      }
      console.log(JSON.stringify(farmCertificateIds));
    }

    console.log("\n\n");
    // paging farmCertificate with limit=10 per page
    let i = 0;
    let counter = 1;
    let from = farmCertificateIds[0];
    while (i < 20) {
      console.log(
        "************************************************FARM CERTIFICATE*************************************************************************"
      );
      console.log(`page ${counter}`);
      let farmCertificateList = await account.queryData("getFarmCertificates", {
        fromField: "Id",
        from: from,
        limit: 10,
        languageId: result.vietnamese,
        order: { field: "CreatedAt", type: 1 },
      }); //ASC
      if (farmCertificateList.data.length === 0) {
        break;
      }

      for (let j = 0; j < farmCertificateList.data.length; j++) {
        console.log(
          account.toObject(
            data.requestType.farmCertificateInfo,
            farmCertificateList.data[j]
          )
        );
      }
      from = farmCertificateList.data[farmCertificateList.data.length - 1].id;
      i += 10;
      counter++;
    }
  } catch (e) {
    console.error(e);
  }
};

main()
  .then(function () {
    console.log(
      "=============================================================================================================================="
    );
  })
  .catch(function (err) {
    console.error(err);
    console.log(
      "=============================================================================================================================="
    );
  });
