const fs = require("fs");
const protobuf = require("protobufjs");

const getProtos = protoPath => {
	var protos = [];
	fs.readdirSync(protoPath)
		.filter(file => (file.slice(-6) === '.proto'))
		.forEach((file) => {
			protos.push(`${protoPath}/${file}`);
		});
	return protos;
};

let files = getProtos(`${__dirname}/../proto`);
if (files.length === 0) {
    return null;
}
protobuf.load(files).then(root => {
    let jsonData = root.toJSON();
    fs.writeFileSync(`${__dirname}/proto.json`, JSON.stringify(jsonData));
});
